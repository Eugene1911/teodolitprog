#include "teodolitprog.h"
#include "ui_teodolitprog.h"
#include <QDate>
#include <QFileDialog>
#include <QFile>
#include <QTextStream>
#include <QTextCursor>
#include <QTextCodec>

TeodolitProg::TeodolitProg(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TeodolitProg)
{
    ui->setupUi(this);
    this->initSerial();
    QDate date;
    date = QDate::currentDate();
    QString strDate = date.toString("yy.MM.dd");
    ui->date->setText(strDate);
    ui->vpp->setInputMask("99/99");
    QString maskLong, maskLat, maskHor, maskVert, maskDir;
    maskDir.append("999"); maskDir.append(QChar(176));
    ui->dirLand->setInputMask("НП-9");
    //ui->dirLand->setText("000");
    maskLong.append("999"); maskLong.append(QChar(176)); maskLong.append("99"); maskLong.append("'"); maskLong.append("99,999999''");
    maskLat.append("99"); maskLat.append(QChar(176)); maskLat.append("99"); maskLat.append("'"); maskLat.append("99,999999''");
    ui->longtitude->setInputMask(maskLong);
    maskHor.append("999"); maskHor.append(QChar(176)); maskHor.append("99"); maskHor.append("'"); maskHor.append("99''");
    maskVert.append("99"); maskVert.append(QChar(176)); maskVert.append("99"); maskVert.append("'"); maskVert.append("99''");
    ui->latitude->setInputMask(maskLat);
    ui->altitude->setInputMask("999,9999");
    ui->vertical->setInputMask(maskVert);
    ui->horizont->setInputMask(maskHor);
    ui->latitude->setText("000000000000");
    ui->longtitude->setText("0000000000000");
    ui->altitude->setText("0000000");
    ui->vertical->setText("0000000");
    ui->horizont->setText("0000000");
}

TeodolitProg::~TeodolitProg()
{
    this->deinitSerial();
    delete ui;
}

void TeodolitProg::initSerial()
{
    serial = new QSerialPort;
    //buf = ui->bufSize->value();
    //buf *= 1024;
    connect (this->serial, SIGNAL(readyRead()), this, SLOT(receive()));
    connect(serial, SIGNAL(error(QSerialPort::SerialPortError)), this, SLOT(serialErr(QSerialPort::SerialPortError)));
}

void TeodolitProg::deinitSerial()
{
    if (this->serial->isOpen()){//закрытие порта
        this->serial->close();
    }
    delete serial;//удаление порта
}

void TeodolitProg::serialErr(QSerialPort::SerialPortError)
{
    QString err;
    ui->er_label->clear();
    if (serial->error() == QSerialPort::DeviceNotFoundError){
        err.append(QString("Failed to found device for port %1, error: %2").arg(serial->portName()).arg(serial->errorString()));
    }
    if (serial->error() == QSerialPort::PermissionError){
        err.append(QString("Failed to permission for port %1, error: %2").arg(serial->portName()).arg(serial->errorString()));
    }
    if (serial->error() == QSerialPort::OpenError){
        err.append(QString("Failed to open for port %1, error: %2").arg(serial->portName()).arg(serial->errorString()));
    }
    if (serial->error() == QSerialPort::NotOpenError){
        err.append(QString("Failed to not open error for port %1, error: %2").arg(serial->portName()).arg(serial->errorString()));
    }
    if (serial->error() == QSerialPort::ParityError){
        err.append(QString("Failed to set no parity for port %1, error: %2").arg(serial->portName()).arg(serial->errorString()));
    }
    if (serial->error() == QSerialPort::FramingError){
        err.append(QString("Failed to framing for port %1, error: %2").arg(serial->portName()).arg(serial->errorString()));
    }
    if (serial->error() == QSerialPort::BreakConditionError){
        err.append(QString("Failed to break condition error for port %1, error: %2").arg(serial->portName()).arg(serial->errorString()));
    }
    if (serial->error() == QSerialPort::WriteError){
        err.append(QString("Failed to write for port %1, error: %2").arg(serial->portName()).arg(serial->errorString()));
    }
    if (serial->error() == QSerialPort::ReadError){
        err.append(QString("Failed to read for port %1, error: %2").arg(serial->portName()).arg(serial->errorString()));
    }
    if (serial->error() == QSerialPort::ResourceError){
        err.append(QString("Recource error for port %1, error: %2").arg(serial->portName()).arg(serial->errorString()));
    }
    if (serial->error() == QSerialPort::UnsupportedOperationError){
        err.append(QString("Unsupported operation error for port %1, error: %2").arg(serial->portName()).arg(serial->errorString()));
    }
    if (serial->error() == QSerialPort::TimeoutError){
        err.append(QString("Timeout error for port %1, error: %2").arg(serial->portName()).arg(serial->errorString()));
    }
    if (serial->error() == QSerialPort::UnknownError){
        err.append(QString("Unknown error for port %1, error: %2").arg(serial->portName()).arg(serial->errorString()));
    }
    ui->er_label->setText(err);
    return;
}

void TeodolitProg::on_openBut_clicked()
{
    if (this->serial->isOpen()){
        serial->close();
        ui->openBut->setText("Открыть");
        ui->er_label->setText(" ");
    }
    else{//конфигурация порта
        QString com, baud, datBits, parity, stops;
        QString parityStr;
        int bd;
        QFile file("portImi.cfg");
        QTextStream input(&file);
        input.setCodec("Windows-1251");
        QString str;
        if(file.open(QIODevice::ReadOnly |QIODevice::Text)){
            int n = 0;
            while (!input.atEnd()){
                str = input.readLine();
                //str = file.readLine();
                if (n == 0)
                    com = str;
                if (n == 1)
                    datBits = str;
                if (n == 2)
                    parity = str;
                if (n == 3)
                    stops = str;
                if (n == 4)
                    baud = str;
                if (n == 5){
                    path = str;
                    //path = CodecWinToUni(CodecLatinFromUni(str));
                }
                n += 1;
            }
        }
        //com = com.left(com.indexOf('\n'));
        bd = baud.toInt();
        serial->setPortName(com);
        if (serial->open(QSerialPort::ReadWrite)){
            serial->setBaudRate(bd);
            if (datBits == "5")
                serial->setDataBits(QSerialPort::Data5);
            if (datBits == "6")
                serial->setDataBits(QSerialPort::Data6);
            if (datBits == "7")
                serial->setDataBits(QSerialPort::Data7);
            if (datBits == "8")
                serial->setDataBits(QSerialPort::Data8);
            if (parity == "Even"){
                //serial->setParity(QSerialPort::EvenParity);
                parityStr = "Even";
                serial->setParity(QSerialPort::EvenParity);
            }
            if (parity == "Odd"){
                parityStr = "Odd";
                serial->setParity(QSerialPort::OddParity);
            }
            if (parity == "No"){
                parityStr = "No";
                serial->setParity(QSerialPort::NoParity);
            }
            if (stops == "1")
                serial->setStopBits(QSerialPort::OneStop);
            if (stops == "2")
                serial->setStopBits(QSerialPort::TwoStop);
            serial->setFlowControl(QSerialPort::NoFlowControl);
            ui->openBut->setText("закрыть");
            //serial->setReadBufferSize(buf);
        }
        else{
            ui->er_label->setText("Error opened serial device");
        }
    }
}

void TeodolitProg::receive()
{
    QString str1, str2;
    QByteArray recDat;
    recDat.append(serial->readAll());
    allDat.append(recDat);
    str1.append(recDat.toHex());

    /*for (int i=0;i<=str1.size(); i+= 2)
        str2.append(QString("%1%2 ").arg(str1[i]).arg(str1[i+1]));*/

    if (allDat[allDat.size()-1]==0x0a){
        allDat.remove(allDat.size()-2, 2);
        allDat.append(0x20); allDat.append(0x20); allDat.append(0x20);
        for (int i=0; i<allDat.size(); i++){
            char sym = allDat[i];
            str2.append(sym);
        }
        ui->workEdit->insertPlainText(str2);
        allDat.clear();
        QTime time;
        time = QTime::currentTime();
        QString strTime = time.toString("hh.mm.ss");
        strTime.append('\n');
        ui->workEdit->insertPlainText(strTime);
        QTextCursor cursor = ui->workEdit->textCursor();
        cursor.movePosition(QTextCursor::End);
        ui->workEdit->setTextCursor(cursor);
    }
    recDat.clear();
}
/*
void TeodolitProg::on_NewFileBut_clicked()
{
    filepath = path;
    filepath.append("\\[");
    filepath.append(ui->date->text());
    filepath.append("-");
    QTime time;
    time = QTime::currentTime();
    QString strTimeCom = time.toString("hh.mm.ss");
    filepath.append(strTimeCom);
    filepath.append("][");

    filepath.append(ui->dirLand->text());
    filepath.append("] teo.txt");
    QFile file(filepath);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;
    QTextStream out(&file);
    QString str;
    //запись позиции
    str = "Позиция : ";
    str.append(ui->position->text());
    out << str;
    out << "\n";
    str.clear();
    //запись ВПП
    str = "ВПП : ";
    str.append("ВПП "); str.append(ui->vpp->text());
    str.append("\n");
    out << str;
    str.clear();
    //запись часового пояса
    str = "Часовой пояс : "; str.append("UTC\n");
    out << str;
    str.clear();
    //запись координат
    str = "позиция              | широта              | долгота             | высота, м | \n";
    out << str;
    out <<"---------------------|---------------------|---------------------|-----------|\n";
    str = "координаты теодолита | N ";
    str.append(ui->latitude->text());
    str.append(" | E ");
    str.append(ui->longtitude->text());
    str.append(" | ");
    str.append(ui->altitude->text()); str.append("  |\n");
    out << str;
    out << "\n";
    str.clear();
    //запись углов ВКУ
    str = "Углы ВКУ\n";
    out << str;
    str.clear(); str = "Вертикаль | Горизонт |\n";
    out << str;
    out << "----------------------\n"; str.clear();
    str.append(" "); str.append(ui->vertical->text()); str.append("  | ");
    str.append(ui->horizont->text()); str.append("  |\n");
    out << str;
    out << "\n\n";
    str.clear();

    //запись данных с теодолита
    str = " дата и время         | вертикаль| горизонт |\n";
    out << str;
    str.clear();
    out <<"---------------------------------------------\n";
    QString strDate, strTime, strVert, strHor, strAll;
    strDate = ui->date->text();
    strAll = ui->workEdit->toPlainText();
    while (strAll.size()>0){
        for (int i=11; i<18; i++)
            strVert.append(strAll[i]);
        for (int i=23; i<30; i++)
            strHor.append(strAll[i]);
        for (int i=32; i<42; i++)
            strTime.append(strAll[i]);
        strAll.remove(0, 43);
        out << strDate; out << " ";
        out << strTime; out << " | ";
        out << strVert; out << "  | ";
        out << strHor;  out << "  |\n";
        strTime.clear(); strVert.clear(); strHor.clear();
    }
    file.close();
    ui->workEdit->clear();
}*/

void TeodolitProg::on_saveBut_clicked()
{
    filepath = path;
    filepath.append("\\[");
    filepath.append(ui->date->text());
    filepath.append("-");
    QString strDate, strTime, strVert, strHor, strAll;
    strDate = ui->date->text();
    strAll = ui->workEdit->toPlainText();
    //QTime time;
    //time = QTime::currentTime();
    //QString strTimeCom = time.toString("hh.mm.ss");
    QString strTimeCom;
    if (strAll.size()>45){
        for (int i=34; i<42; i++)
            strTimeCom.append(strAll[i]);
    }
    else strTimeCom = " ";
    filepath.append(strTimeCom);
    filepath.append("][");

    filepath.append(ui->dirLand->text());
    filepath.append("][");
    if (ui->Curs->isChecked())
        filepath.append("К");
    else filepath.append("Г");
    filepath.append("] teo.sav");
    QString filename;
    filename = QFileDialog::getSaveFileName(
                this,
                tr("open file"),
                filepath,
                "Txt files (*.txt);;"
                );
    filepath = filename;
    int ind = filename.lastIndexOf('/');
    for (int i=filename.size(); i>ind; i--)
        filepath = filepath.remove(i, 1);
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;
    QTextStream out(&file);
    QString str;
    //запись позиции
    str = "Позиция : ";
    str.append(ui->position->text());
    out << str;
    out << "\n";
    str.clear();
    //запись ВПП
    str = "ВПП : ";
    str.append("ВПП "); str.append(ui->vpp->text());
    str.append("\n");
    out << str;
    str.clear();
    //запись часового пояса
    str = "Часовой пояс : "; str.append("UTC\n\n");
    out << str;
    str.clear();
    //запись координат
    str = "позиция              | широта              | долгота              | высота, м | \n";
    out << str;
    out <<"---------------------|---------------------|----------------------|-----------|\n";
    str = "координаты теодолита | N ";
    str.append(ui->latitude->text());
    str.append(" | E ");
    str.append(ui->longtitude->text());
    str.append(" | ");
    str.append(ui->altitude->text()); str.append("  |\n");
    out << str;
    out << "\n";
    str.clear();
    //запись углов ВКУ
    str = "Углы ВКУ\n";
    out << str;
    str.clear(); str = "Вертикаль     | Горизонт     |\n";
    out << str;
    out << "------------------------------\n"; str.clear();
    str.append(" "); str.append(ui->vertical->text()); str.append("   | ");
    str.append(ui->horizont->text()); str.append("  |\n");
    out << str;
    out << "\n\n";
    str.clear();

    //запись данных с теодолита
    str = " время   | вертикаль| горизонт |\n";
    out << str;
    str.clear();
    out <<"--------------------------------\n";

    while (strAll.size()>0){
        for (int i=11; i<18; i++)
            strVert.append(strAll[i]);
        for (int i=23; i<30; i++)
            strHor.append(strAll[i]);
        for (int i=34; i<42; i++)
            strTime.append(strAll[i]);
        strAll.remove(0, 43);
        //out << strDate; out << " ";
        out << strTime; out << " | ";
        out << strVert; out << "  | ";
        out << strHor;  out << "  |\n";
        strTime.clear(); strVert.clear(); strHor.clear();
    }
    file.close();
    ui->workEdit->clear();
}


