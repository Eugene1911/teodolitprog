#ifndef TEODOLITPROG_H
#define TEODOLITPROG_H

#include <QMainWindow>
#include <QtSerialPort/QtSerialPort>

namespace Ui {
class TeodolitProg;
}

class TeodolitProg : public QMainWindow
{
    Q_OBJECT

public:
    explicit TeodolitProg(QWidget *parent = 0);
    ~TeodolitProg();

private slots:
    void receive();

    void on_openBut_clicked();

    void on_saveBut_clicked();

public slots:
    void serialErr(QSerialPort::SerialPortError);

private:
    Ui::TeodolitProg *ui;
    QSerialPort *serial;
    void initSerial();
    void deinitSerial();
    QString path, filepath;
    QByteArray allDat;
};

#endif // TEODOLITPROG_H
