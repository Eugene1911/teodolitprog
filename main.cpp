#include "teodolitprog.h"
#include <QApplication>
#include <QTextCodec>

#define CodecWinToUni(str1) CodecWinToUni->toUnicode(str1)
#define CodecUtfToUni(str1) CodecUtfToUni->toUnicode(str1)
#define CodecLatinFromUni(str1) codecLatin->fromUnicode(str1)

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QTextCodec *CodecWinToUni; //кодеки для работы в программе
    QTextCodec *CodecUtfToUni;
    QTextCodec *codecLatin;
    CodecWinToUni = QTextCodec::codecForName("Windows-1251");
    CodecUtfToUni = QTextCodec::codecForName("UTF-8");
    codecLatin = QTextCodec::codecForName("ISO 8859-1");
    TeodolitProg w;
    w.show();

    return a.exec();
}
