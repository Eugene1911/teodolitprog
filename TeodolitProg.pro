#-------------------------------------------------
#
# Project created by QtCreator 2017-06-15T11:53:55
#
#-------------------------------------------------

QT       += core gui
QT       += serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TeodolitProg
TEMPLATE = app


SOURCES += main.cpp\
        teodolitprog.cpp

HEADERS  += teodolitprog.h

FORMS    += teodolitprog.ui
